package org.example.multiplication;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.example.exceptions.InvalidPropertyValueException;
import org.example.exceptions.ValueOutOfBoundsException;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static org.example.App.LOGGER;

public class ByteMultiplicationTable extends MultiplicationTable {
    private byte min;
    private byte max;
    private byte increment;
    public ByteMultiplicationTable(Properties configuration) {
        super(configuration);
    }

    @Override
    public void validateConfiguration(Properties configuration) {
        min = getByte(configuration.getProperty("min"));
        max = getByte(configuration.getProperty("max"));
        increment = getByte(configuration.getProperty("increment"));

        if (!configurationIsCorrect(min,max,increment)) {
            throw new InvalidPropertyValueException();
        }

        LOGGER.debug("Loaded properties have correct types and values");
    }
    public byte getByte(String line) {
        try {
            return Byte.parseByte(line);
        } catch (NumberFormatException e) {
            LOGGER.error("Incorrect type of properties (BYTE REQUIRED). {}", ExceptionUtils.getStackTrace(e));
            throw new InvalidPropertyValueException();
        }
    }

    @Override
    protected List<String> generateMultiplicationTableLines() {
        List<String> list = new LinkedList<>();

        if (increment > 0) {
            for (byte i = min; i <= max; i += increment) {
                for (byte j = min; j <= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        } else {
            for (byte i = min; i >= max; i += increment) {
                for (byte j = min; j >= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        }
        return list;
    }

    public byte multiply(byte num1, byte num2) {
        long result = (long)num1 * num2;
        if (result > Byte.MAX_VALUE || result < Byte.MIN_VALUE) {
            LOGGER.error("Result of multiplication out of bounds");
            throw new ValueOutOfBoundsException();
        }
        return (byte)result;
    }
}
