package org.example.multiplication;

import java.util.List;
import java.util.Properties;

import static org.example.App.LOGGER;

public abstract class MultiplicationTable {
    protected MultiplicationTable(Properties properties) {
        validateConfiguration(properties);
        LOGGER.debug("{} was created successfully", this.getClass().getSimpleName());
    }

    protected abstract void validateConfiguration(Properties properties);

    protected abstract List<String> generateMultiplicationTableLines();

    protected boolean configurationIsCorrect(double min, double max, double increment) {
        if (increment == 0) {
            LOGGER.error("Increment value could not be zero");
            return false;
        } else if ((min > max && increment > 0) || (min < max && increment < 0)) {
            LOGGER.error("Increment direction is incorrect");
            return false;
        } else if (Math.abs(increment) > Math.abs(max - min)) {
            LOGGER.error("Increment value is too large for given range");
            return false;
        }
        return true;
    }

    protected String formatMultiplicationLine(double num1, double num2, double result) {
        return num1 + " * " + num2 + " = " + result;
    }

    public void printTable() {
        generateMultiplicationTableLines().forEach(LOGGER::info);
    }
}
