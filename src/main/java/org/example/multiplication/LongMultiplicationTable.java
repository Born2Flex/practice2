package org.example.multiplication;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.example.exceptions.InvalidPropertyValueException;
import org.example.exceptions.ValueOutOfBoundsException;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static org.example.App.LOGGER;

public class LongMultiplicationTable extends MultiplicationTable {
    private long min;
    private long max;
    private long increment;
    public LongMultiplicationTable(Properties configuration) {
        super(configuration);
    }

    @Override
    public void validateConfiguration(Properties configuration) {
        min = getLong(configuration.getProperty("min"));
        max = getLong(configuration.getProperty("max"));
        increment = getLong(configuration.getProperty("increment"));

        if (!configurationIsCorrect(min,max,increment)) {
            throw new InvalidPropertyValueException();
        }

        LOGGER.debug("Loaded properties have correct types and values");
    }

    private long getLong(String line) {
        try {
            return Long.parseLong(line);
        } catch (NumberFormatException e) {
            LOGGER.error("Incorrect type of properties (LONG REQUIRED). {}", ExceptionUtils.getStackTrace(e));
            throw new InvalidPropertyValueException();
        }
    }

    @Override
    protected List<String> generateMultiplicationTableLines() {
        List<String> list = new LinkedList<>();

        if (increment > 0) {
            for (long i = min; i <= max; i += increment) {
                for (long j = min; j <= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        } else {
            for (long i = min; i >= max; i += increment) {
                for (long j = min; j >= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        }
        return list;
    }

    public long multiply(long num1, long num2) {
        BigInteger result = BigInteger.valueOf(num1);
        result = result.multiply(BigInteger.valueOf(num2));
        if (result.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0
                || result.compareTo(BigInteger.valueOf(Long.MIN_VALUE)) < 0) {
            LOGGER.error("Result of multiplication out of bounds");
            throw new ValueOutOfBoundsException();
        }
        return result.longValue();
    }
}
