package org.example.multiplication;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.example.exceptions.InvalidPropertyValueException;
import org.example.exceptions.ValueOutOfBoundsException;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static org.example.App.LOGGER;

public class ShortMultiplicationTable extends MultiplicationTable{
    private short min;
    private short max;
    private short increment;
    public ShortMultiplicationTable(Properties configuration) {
        super(configuration);
    }

    @Override
    public void validateConfiguration(Properties configuration) {
        min = getShort(configuration.getProperty("min"));
        max = getShort(configuration.getProperty("max"));
        increment = getShort(configuration.getProperty("increment"));

        if (!configurationIsCorrect(min,max,increment)) {
            throw new InvalidPropertyValueException();
        }

        LOGGER.debug("Loaded properties have correct types and values");
    }

    short getShort(String line) {
        try {
            return Short.parseShort(line);
        } catch (NumberFormatException e) {
            LOGGER.error("Incorrect type of properties (SHORT REQUIRED). {}", ExceptionUtils.getStackTrace(e));
            throw new InvalidPropertyValueException();
        }
    }

    @Override
    protected List<String> generateMultiplicationTableLines() {
        List<String> list = new LinkedList<>();

        if (increment > 0) {
            for (short i = min; i <= max; i += increment) {
                for (short j = min; j <= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        } else {
            for (short i = min; i >= max; i += increment) {
                for (short j = min; j >= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        }
        return list;
    }

    public short multiply(short num1, short num2) {
        long result = (long)num1 * num2;
        if (result > Short.MAX_VALUE || result < Short.MIN_VALUE) {
            LOGGER.error("Result of multiplication out of bounds");
            throw new ValueOutOfBoundsException();
        }
        return (short)result;
    }
}
