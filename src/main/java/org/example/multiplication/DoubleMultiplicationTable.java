package org.example.multiplication;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.example.exceptions.InvalidPropertyValueException;
import org.example.exceptions.ValueOutOfBoundsException;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static org.example.App.LOGGER;

public class DoubleMultiplicationTable extends MultiplicationTable {
    private double min;
    private double max;
    private double increment;
    public DoubleMultiplicationTable(Properties configuration) {
        super(configuration);
    }

    @Override
    public void validateConfiguration(Properties configuration) {
        min = getDouble(configuration.getProperty("min"));
        max = getDouble(configuration.getProperty("max"));
        increment = getDouble(configuration.getProperty("increment"));

        if (!configurationIsCorrect(min,max,increment)) {
            throw new InvalidPropertyValueException();
        }

        LOGGER.debug("Loaded properties have correct types and values");
    }

    private double getDouble(String line) {
        try {
            return Double.parseDouble(line);
        } catch (NumberFormatException e) {
            LOGGER.error("Incorrect type of properties (DOUBLE REQUIRED). {}", ExceptionUtils.getStackTrace(e));
            throw new InvalidPropertyValueException();
        }
    }

    @Override
    protected List<String> generateMultiplicationTableLines() {
        List<String> list = new LinkedList<>();

        if (increment > 0) {
            for (double i = min; i <= max; i += increment) {
                for (double j = min; j <= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        } else {
            for (double i = min; i >= max; i += increment) {
                for (double j = min; j >= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        }
        return list;
    }

    public double multiply(double num1, double num2) {
        BigDecimal result = BigDecimal.valueOf(num1);
        result = result.multiply(BigDecimal.valueOf(num2));
        if (result.compareTo(BigDecimal.valueOf(Long.MAX_VALUE)) > 0
                || result.compareTo(BigDecimal.valueOf(Long.MIN_VALUE)) < 0) {
            LOGGER.error("Result of multiplication out of bounds");
            throw new ValueOutOfBoundsException();
        }
        return result.longValue();
    }
}
