package org.example.multiplication;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.example.exceptions.InvalidPropertyValueException;
import org.example.exceptions.ValueOutOfBoundsException;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static org.example.App.LOGGER;

public class IntegerMultiplicationTable extends MultiplicationTable {
    private int min;
    private int max;
    private int increment;
    public IntegerMultiplicationTable(Properties configuration) {
        super(configuration);
    }

    @Override
    public void validateConfiguration(Properties configuration) {
        min = getInt(configuration.getProperty("min"));
        max = getInt(configuration.getProperty("max"));
        increment = getInt(configuration.getProperty("increment"));

        if (!configurationIsCorrect(min,max,increment)) {
            throw new InvalidPropertyValueException();
        }

        LOGGER.debug("Loaded properties have correct types and values");
    }

    private int getInt(String line) {
        try {
            return Integer.parseInt(line);
        } catch (NumberFormatException e) {
            LOGGER.error("Incorrect type of properties (INTEGER REQUIRED). {}", ExceptionUtils.getStackTrace(e));
            throw new InvalidPropertyValueException();
        }
    }

    @Override
    protected List<String> generateMultiplicationTableLines() {
        List<String> list = new LinkedList<>();

        if (increment > 0) {
            for (int i = min; i <= max; i += increment) {
                for (int j = min; j <= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        } else {
            for (int i = min; i >= max; i += increment) {
                for (int j = min; j >= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        }
        return list;
    }

    public int multiply(int num1, int num2) {
        long result = (long)num1 * num2;
        if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE) {
            LOGGER.error("Result of multiplication out of bounds");
            throw new ValueOutOfBoundsException();
        }
        return (int)result;
    }
}
