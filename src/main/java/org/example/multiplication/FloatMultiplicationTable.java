package org.example.multiplication;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.example.exceptions.InvalidPropertyValueException;
import org.example.exceptions.ValueOutOfBoundsException;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import static org.example.App.LOGGER;

public class FloatMultiplicationTable extends MultiplicationTable {
    private float min;
    private float max;
    private float increment;
    public FloatMultiplicationTable(Properties configuration) {
        super(configuration);
    }

    @Override
    public void validateConfiguration(Properties configuration) {
        min = getFloat(configuration.getProperty("min"));
        max = getFloat(configuration.getProperty("max"));
        increment = getFloat(configuration.getProperty("increment"));

        if (!configurationIsCorrect(min,max,increment)) {
            throw new InvalidPropertyValueException();
        }

        LOGGER.debug("Loaded properties have correct types and values");
    }

    private float getFloat(String line) {
        try {
            return Float.parseFloat(line);
        } catch (NumberFormatException e) {
            LOGGER.error("Incorrect type of properties (FLOAT REQUIRED). {}", ExceptionUtils.getStackTrace(e));
            throw new InvalidPropertyValueException();
        }
    }

    @Override
    protected List<String> generateMultiplicationTableLines() {
        List<String> list = new LinkedList<>();

        if (increment > 0) {
            for (float i = min; i <= max; i += increment) {
                for (float j = min; j <= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        } else {
            for (float i = min; i >= max; i += increment) {
                for (float j = min; j >= max; j += increment) {
                    list.add(formatMultiplicationLine(i, j, multiply(i, j)));
                }
            }
        }
        return list;
    }

    public float multiply(float num1, float num2) {
        double result = (double)num1 * num2;
        if (result > Float.MAX_VALUE || result < Float.MIN_VALUE) {
            LOGGER.error("Result of multiplication out of bounds");
            throw new ValueOutOfBoundsException();
        }
        return (float)result;
    }
}
