package org.example;

import org.example.multiplication.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class App {
    public static final Logger LOGGER = LoggerFactory.getLogger(App.class.getCanonicalName());
    private static final String DEFAULT_TABLE_TYPE = "int";
    public static void main( String[] args ) {
        LOGGER.info("Start of program");
        App app = new App();
        String tableType = System.getProperty("type");
        Properties properties = new PropertiesLoader().loadProperties();
        MultiplicationTable table = app.getMultiplicationTable(tableType,properties);
        table.printTable();
        LOGGER.info("End");
    }

    private MultiplicationTable getMultiplicationTable(String tableType, Properties properties) {
        if (tableType == null) {
            LOGGER.debug("Multiplication table type not given, using default");
            tableType = DEFAULT_TABLE_TYPE;
        }
        tableType = tableType.toLowerCase();
        LOGGER.info("Chosen table type: {}",tableType);
        switch (tableType) {
            case "byte":
                return new ByteMultiplicationTable(properties);
            case "short":
                return new ShortMultiplicationTable(properties);
            case "long":
                return new LongMultiplicationTable(properties);
            case "float":
                return new FloatMultiplicationTable(properties);
            case "double":
                return new DoubleMultiplicationTable(properties);
            default:
                return new IntegerMultiplicationTable(properties);
        }
    }
}
