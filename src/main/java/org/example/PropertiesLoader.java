package org.example;

import org.example.exceptions.NoSuchPropertyException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import static org.example.App.LOGGER;

public class PropertiesLoader {
    private static final String CONFIG_FILENAME = "config.properties";

    public Properties loadProperties() {
        File file = new File(CONFIG_FILENAME);
        Properties properties = new Properties();

        if (file.exists()) {
            LOGGER.debug("Using outer properties file");
            try (InputStreamReader reader = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)) {
                properties.load(reader);
            } catch (IOException e) {
                LOGGER.warn("An error occurred while loading outer properties: {}", e.getMessage());
            }
        } else {
            LOGGER.debug("Using inner properties file");
            try (InputStream resources = App.class.getClassLoader().getResourceAsStream(CONFIG_FILENAME)) {
                properties.load(resources);
            } catch (IOException e) {
                LOGGER.warn("An error occurred while loading inner properties: {}", e.getMessage());
            }
        }
        checkLoadedProperties(properties);
        return properties;
    }

    private void checkLoadedProperties(Properties properties) {
        String[] requiredProperties = {"min", "max", "increment"};

        for (String property : requiredProperties) {
            if (!properties.containsKey(property)) {
                LOGGER.error("Property \"{}\" NOT present in properties file",property);
                throw new NoSuchPropertyException();
            }
        }
    }
}
