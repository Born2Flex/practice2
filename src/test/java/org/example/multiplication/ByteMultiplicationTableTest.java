package org.example.multiplication;

import org.example.exceptions.InvalidPropertyValueException;
import org.example.exceptions.ValueOutOfBoundsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ByteMultiplicationTableTest {
    ByteMultiplicationTable table;
    Properties properties;

    @BeforeEach
    void prepare() {
        properties = mock(Properties.class);
        when(properties.getProperty("min")).thenReturn("0");
        when(properties.getProperty("max")).thenReturn("1");
        when(properties.getProperty("increment")).thenReturn("1");
        table = new ByteMultiplicationTable(properties);
    }

    @ParameterizedTest
    @CsvSource(value = {"-128", "0", "127", "35", "-10"})
    void getByteTest(String num) {
        assertDoesNotThrow(() -> table.getByte(num));
    }

    @ParameterizedTest
    @CsvSource(value = {"-129", "335", "128", "-110.0"})
    void getByteTest_IncorrectValue(String num) {
        assertThrows(InvalidPropertyValueException.class, () -> table.getByte(num));
    }

    @ParameterizedTest
    @CsvSource(value = {"2,3,6", "-10,-11,110", "-33,3,-99", "14,-2,-28", "-28,-3,84"})
    void multiplicationTest(byte num1, byte num2, byte result) {
        assertEquals(result, table.multiply(num1, num2));
    }

    @ParameterizedTest
    @CsvSource(value = {"20,-10", "64,2", "10,-13"})
    void multiplicationTest_ResultOutOfBounds(byte num1, byte num2) {
        assertThrows(ValueOutOfBoundsException.class, () -> table.multiply(num1, num2));
    }
}