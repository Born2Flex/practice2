package org.example.multiplication;

import org.example.exceptions.InvalidPropertyValueException;
import org.example.exceptions.ValueOutOfBoundsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import java.util.Properties;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ShortMultiplicationTableTest {
    ShortMultiplicationTable table;
    Properties properties;
    @BeforeEach
    void prepare() {
        properties = mock(Properties.class);
        when(properties.getProperty("min")).thenReturn("0");
        when(properties.getProperty("max")).thenReturn("1");
        when(properties.getProperty("increment")).thenReturn("1");
        table = new ShortMultiplicationTable(properties);
    }
    @ParameterizedTest
    @CsvSource(value = {"-32768","0","32767","31527","-17852"})
    void getByteTest(String num) {
        assertDoesNotThrow(() -> table.getShort(num));
    }
    @ParameterizedTest
    @CsvSource(value = {"-32769","32768","85214","-100.0"})
    void getByteTest_IncorrectValue(String num) {
        assertThrows(InvalidPropertyValueException.class,() -> table.getShort(num));
    }
    @ParameterizedTest
    @CsvSource(value = {"2,3,6","225,62,13950","8192,-4,-32768","14,-2,-28","-28,-3,84"})
    void multiplicationTest(short num1, short num2, short result) {
        assertEquals(result,table.multiply(num1,num2));
    }
    @ParameterizedTest
    @CsvSource(value = {"8193,-4,-32768","64,2025","357,275"})
    void multiplicationTest_ResultOutOfBounds(short num1, short num2) {
        assertThrows(ValueOutOfBoundsException.class,() -> table.multiply(num1,num2));
    }
}