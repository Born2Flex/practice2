package org.example.multiplication;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MultiplicationTableTest {
    MultiplicationTable table;
    @BeforeEach
    void prepare() {
        table = new MultiplicationTable(new Properties()) {
            @Override
            public void validateConfiguration(Properties properties) {
            }

            @Override
            protected List<String> generateMultiplicationTableLines() {
                return null;
            }
            @Override
            public void printTable() {
            }
        };
    }
    @ParameterizedTest
    @CsvSource(value = {"0,100,5","-5,25,10","-99,9,3","100,125,15","435,-45,-85","-128,127,55",
                        "-32,1000,456","32767,-32768,-25","456,-3569,-35","753,951,12","31132,360,-1000"})
    void configurationIsCorrectTest_CorrectValues(double min, double max, double increment) {
        assertTrue(table.configurationIsCorrect(min, max, increment));
    }
    @ParameterizedTest
    @CsvSource(value = {"-356,10000,-5","-32768,-25,32767","-15555,-4,88888","5,30712,-3","3678,228,3333",
            "0,100,-5","-5,4,10","-99,9,120","100,145,-15","-155,-4,-8","1.0,2,-1","5,10.0,-3","6,18,-3.0"})
    void configurationIsCorrectTest_IncorrectValues(double min, double max, double increment) {
        assertFalse(table.configurationIsCorrect(min, max, increment));
    }
}